---
layout: default
title: When was I conceived if I was born on May 25, 1955?
birthday: May 25, 1955
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



