---
layout: default
title: When was I conceived if I was born on January 30, 1950?
birthday: Jan 30, 1950
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



