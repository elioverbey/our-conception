---
layout: default
title: When was I conceived if I was born on February 16, 1950?
birthday: Feb 16, 1950
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



