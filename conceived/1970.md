---
layout: conceived
title: When was I conceived if I was born in 1970?
---


## Birth Month


- [January](1970/01)
- [February](1970/02)
- [March](1970/03)
- [April](1970/04)
- [May](1970/05)
- [June](1970/06)
- [July](1970/07)
- [August](1970/08)
- [September](1970/09)
- [October](1970/10)
- [November](1970/11)
- [December](1970/12)

{% include nav.md %}
