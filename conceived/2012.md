---
layout: conceived
title: When was I conceived if I was born in 2012?
---


## Birth Month


- [January](2012/01)
- [February](2012/02)
- [March](2012/03)
- [April](2012/04)
- [May](2012/05)
- [June](2012/06)
- [July](2012/07)
- [August](2012/08)
- [September](2012/09)
- [October](2012/10)
- [November](2012/11)
- [December](2012/12)

{% include nav.md %}
