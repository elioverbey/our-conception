---
layout: default
title: When was I conceived if I was born on February 28, 1974?
birthday: Feb 28, 1974
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



