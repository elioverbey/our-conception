---
layout: default
title: When was my baby conceived if they were born on February 28, 2004?
birthday: Feb 28, 2004
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



