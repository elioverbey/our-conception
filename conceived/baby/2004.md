---
layout: conceived
title: When was my baby conceived if they were born in 2004?
---


## Birth Month


- [January](2004/01)
- [February](2004/02)
- [March](2004/03)
- [April](2004/04)
- [May](2004/05)
- [June](2004/06)
- [July](2004/07)
- [August](2004/08)
- [September](2004/09)
- [October](2004/10)
- [November](2004/11)
- [December](2004/12)

{% include nav.md %}
