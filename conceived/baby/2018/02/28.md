---
layout: default
title: When was my baby conceived if they were born on February 28, 2018?
birthday: Feb 28, 2018
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



