---
layout: default
title: When was I conceived if I was born on February 1, 1965?
birthday: Feb 01, 1965
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



