---
layout: default
title: When was I conceived if I was born on May 30, 1952?
birthday: May 30, 1952
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



