---
layout: default
title: When was I conceived if I was born on March 1, 1952?
birthday: Mar 01, 1952
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



