---
layout: default
title: When was I conceived if I was born on January 19, 1952?
birthday: Jan 19, 1952
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



