---
layout: default
title: When was I conceived if I was born on February 14, 1952?
birthday: Feb 14, 1952
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



