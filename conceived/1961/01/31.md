---
layout: default
title: When was I conceived if I was born on January 31, 1961?
birthday: Jan 31, 1961
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



