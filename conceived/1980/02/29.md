---
layout: default
title: When was I conceived if I was born on February 29, 1980?
birthday: Feb 29, 1980
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



