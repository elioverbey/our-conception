---
layout: default
title: When was I conceived if I was born on December 31, 1963?
birthday: Dec 31, 1963
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



