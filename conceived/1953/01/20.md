---
layout: default
title: When was I conceived if I was born on January 20, 1953?
birthday: Jan 20, 1953
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



