---
layout: default
title: When was I conceived if I was born on May 31, 1953?
birthday: May 31, 1953
---

{% include header.md %}

{% include guess.md %}

{% include calculate.md %}

{% include safety.md %}

{% include footer.md %}



